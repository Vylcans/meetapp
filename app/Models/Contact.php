<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * Based on ordering priorities
     */
    const STATUS_FRIENDS = 0;
    const STATUS_PENDING = 1;
    const STATUS_BLOCKED = 2;
    const STATUS_NONE = 3;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'contact_id',
        'status',
    ];
}
