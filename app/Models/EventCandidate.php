<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventCandidate extends Model {

    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_DECLINED = 2;

}
