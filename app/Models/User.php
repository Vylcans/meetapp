<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    const ROLE_ADMIN = 0;
    const ROLE_USER = 1;

    const STATUS_ACTIVE = 0;
    const GENDER_MALE = 0;
    const GENDER_FEMALE = 1;

    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'facebook_id',
        'description',
        'role',
        'status',
        'gender',
        'birth_date',
        'latitude',
        'longitude',
        'search_radius',
        'search_gender',
        'search_min_age',
        'search_max_age'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public static function getCurrentUser()
    {
        return \Auth::user();
    }

    public static function generatePassword()
    {
        return 'secret';
    }

    public function contacts()
    {
        return $this->belongsToMany('App\Models\User', 'contacts', 'user_id', 'contact_id')->withPivot('status');
    }

    public function scopeFriends($query)
    {
        return $query->where('contacts.status', '=', Contact::STATUS_FRIENDS);
    }

    public function events()
    {
        return $this->hasMany('App\Models\Event');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }

    public function getGenderTranslation($genderType = 'gender')
    {
        $translation = \Lang::get('texts.male');
        if ($this->$genderType == self::GENDER_FEMALE) {
            $translation = \Lang::get('texts.female');
        }

        return $translation;
    }

    /**
     * Gathers data for profile view
     *
     * @return array
     */
    public function gatherProfileData()
    {
        $birthDate = $this->birth_date;
        $year = null;
        $month = null;
        $day = null;
        if ($birthDate) {
            list($year, $month, $day) = explode('-', $birthDate);
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'description' => $this->description,
            'gender' => $this->getGenderTranslation(),
            'birthDate' => [
                'year' => $year,
                'month' => $month,
                'day' => $day
            ],
            'searchGender' => $this->getGenderTranslation('search_gender'),
            'searchRadius' => $this->search_radius,
            'searchMinAge' => $this->search_min_age,
            'searchMaxAge' => $this->search_max_age,
            'values' => [
                'genderFemale' => self::GENDER_FEMALE,
                'genderMale' => self::GENDER_MALE
            ],
            'friendsCount' => $this->contacts()->friends()->count(),
        ];
    }

    public function getContactsList()
    {
        $contacts = $this->contacts()
            ->orderBy('contacts.status')
            ->paginate();

        foreach ($contacts as $contact) {
            //because ->select() has no impact @todo check!
            unset($contact->latitude);
            unset($contact->longitude);
            unset($contact->facebook_id);
            unset($contact->role);
            unset($contact->search_radius);
            unset($contact->search_gender);
            unset($contact->search_min_age);
            unset($contact->search_max_age);
            unset($contact->created_at);
            unset($contact->updated_at);
        }

        return $contacts->toArray();
    }

    public function getUpcomingEvents()
    {
        $friendsIdList = $this->contacts()->friends()->select(['users.id'])->lists('id');

        return Event::whereIn('events.user_id', $friendsIdList)
            ->where('date', '>', new \DateTime())
            //@todo pārbaudi, lai neņem tos, kuriem jau ir akceptēts vai noraidīts
            ->orderBy('date', 'asc')
            ->paginate();
    }

    public function getActiveEvents()
    {
        return $this->events()
            ->where('date', '>', new \DateTime())
            ->orderBy('date', 'asc')
            ->paginate();
    }

    public function getAttendingEvents()
    {
        return Event::where('date', '>', new \DateTime())
            ->whereHas('candidates', function ($q) {
                $q->where('users.id', '=', $this->id)
                    ->where('event_candidates.status', '=', EventCandidate::STATUS_APPROVED)
                ;
            })
            ->orderBy('date', 'asc')
            ->paginate();
    }
}
