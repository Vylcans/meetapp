<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventMessage extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'event_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'user_id',
        'event_id',
    ];

}
