<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

    const STATUS_NEW = 0;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'location',
        'address',
        'description',
        'purpose',
        'status',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function candidates()
    {
        return $this->belongsToMany('App\Models\User', 'event_candidates', 'event_id', 'user_id')->withPivot('status');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\EventMessage');
    }

    /**
     * Used in all events list
     * @return object
     */
    public function leaveBasicData()
    {
        return (object) [
            'id' => $this->id,
            'author' => $this->user->name,
            'description' => $this->description,
            'accepted_candidates' => $this->candidates()
                ->where('event_candidates.status', '=', EventCandidate::STATUS_APPROVED)
                ->count()

        ];
    }

    /**
     * Used in created/attending events list
     * @return object
     */
    public function leaveListData()
    {
        return (object) [
            'id' => $this->id,
            'author' => $this->user->name,
            'description' => $this->description,
            'accepted_candidates' => $this->candidates()
                ->where('event_candidates.status', '=', EventCandidate::STATUS_APPROVED)
                ->count(),
            'messages' => $this->messages()->count()

        ];
    }
}
