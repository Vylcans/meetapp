<?php namespace App\Http\Controllers;

use App\Models\User;
use Laravel\Socialite\Facades\Socialite;

class FacebookController extends Controller
{

    /**
     * Redirects user to authentication on Facebook
     *
     * @return mixed
     */
    public function getAuthenticate()
    {
        return Socialite::with('facebook')
            ->scopes([
                'public_profile',
                'user_friends',
                'user_birthday'
            ])
            ->redirect();
    }

    /**
     * Returning from Facebook action
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getRedirect()
    {
        $fbUser = Socialite::with('facebook')
            ->fields([
                'first_name', 'last_name', 'email', 'gender', 'birthday'
            ])
            ->user();

        $fbId = $fbUser->getId();
        $email = $fbUser->getEmail();

        $fbGender = $fbUser->user['gender'];
        $gender = User::GENDER_FEMALE;
        $searchGender = User::GENDER_MALE;
        if ($fbGender == 'male') {
            $gender = User::GENDER_MALE;
            $searchGender = User::GENDER_FEMALE;
        }

        $user = User::where('facebook_id', '=', $fbId)->first();

        if (!$user) {

            $birthday = $fbUser->user['birthday'];
            $birthday = \DateTime::createFromFormat('m/d/Y', $birthday);

            $user = User::create([
                'name' => $fbUser->name,
                'email' => $email,
                'password' => \Hash::make(User::generatePassword()),
                'facebook_id' => $fbId,
                'gender' => $gender,
                'birth_date' => $birthday,
                'search_gender' => $searchGender,
            ]);

        }

        \Auth::login($user); //todo vēlāk noņemt

        return \Redirect::route('userProfile');
    }
}
