<?php namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class HomeController extends Controller {

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        return JsonResponse::create([
            'status' => 'ok'
        ]);
    }

}
