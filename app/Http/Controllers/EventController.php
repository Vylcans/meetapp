<?php namespace App\Http\Controllers;

use App\Models\User;
use Symfony\Component\HttpFoundation\JsonResponse;

class EventController extends Controller
{

    /**
     * @var null|User
     */
    protected $user;

    public function __construct()
    {
        $this->user = User::getCurrentUser();
    }

    public function getUpcoming()
    {
        $events = $this->user->getUpcomingEvents();

        $events->transform(function ($event) {
            return $event->leaveBasicData();
        });

        return JsonResponse::create([
            'data' => $events->toArray(),
            'values' => []
        ]);
    }

    public function getCreated()
    {
        $events = $this->user->getActiveEvents();

        $events->transform(function ($event) {
            return $event->leaveListData();
        });

        return JsonResponse::create([
            'data' => $events->toArray(),
            'values' => []
        ]);
    }

    public function getAttending()
    {
        $events = $this->user->getAttendingEvents();

        $events->transform(function ($event) {
            return $event->leaveListData();
        });

        return JsonResponse::create([
            'data' => $events->toArray(),
            'values' => []
        ]);
    }
}
