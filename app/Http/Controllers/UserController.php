<?php namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\User;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller
{

    /**
     * @var null|User
     */
    protected $user;

    public function __construct()
    {
        $this->user = User::getCurrentUser();
    }

    /**
     * User's profile data
     *
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    public function getProfile()
    {
        return JsonResponse::create([
            'data' => $this->user->gatherProfileData()
        ]);
    }

    /**
     * Gets contact list for the user
     *
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    public function getContacts()
    {
        return JsonResponse::create([
            'data' => $this->user->getContactsList(),
            'values' => [
                'statuses' => [
                    'friends' => Contact::STATUS_FRIENDS,
                    'pending' => Contact::STATUS_PENDING,
                    'blocked' => Contact::STATUS_BLOCKED,
                    'none' => Contact::STATUS_NONE
                ]
            ]
        ]);
    }
}
