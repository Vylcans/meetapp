<?php

Route::get('/', 'HomeController@index');

Route::group([
    'prefix' => 'facebook',
    'middleware' => [
        //'auth'
    ],
], function () {
    Route::get('/authenticate', [
        'as' => 'facebookAuthenticate',
        'uses' => 'FacebookController@getAuthenticate'
    ]);
    Route::get('/redirect', [
        'as' => 'facebookRedirect',
        'uses' => 'FacebookController@getRedirect'
    ]);
});

Route::group([
    'prefix' => 'user',
    'middleware' => [
        //'auth'
    ],
], function () {
    Route::get('/profile', [
        'as' => 'userProfile',
        'uses' => 'UserController@getProfile'
    ]);
    Route::get('/contacts', [
        'as' => 'userContacts',
        'uses' => 'UserController@getContacts'
    ]);

});

Route::group([
    'prefix' => 'events',
    'middleware' => [
        //'auth'
    ],
], function () {
    Route::get('/upcoming', [
        'as' => 'upcomingEvents',
        'uses' => 'EventController@getUpcoming'
    ]);
    Route::get('/created', [
        'as' => 'createdEvents',
        'uses' => 'EventController@getCreated'
    ]);
    Route::get('/attending', [
        'as' => 'attendingEvents',
        'uses' => 'EventController@getAttending'
    ]);

});