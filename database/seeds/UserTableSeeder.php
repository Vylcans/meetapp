<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class UserTableSeeder extends Seeder
{

    public function run()
    {

        $admin = \App\Models\User::where('role', '=', \App\Models\User::ROLE_ADMIN)->first();

        $users = [];

        if (!$admin) {
            $users[] = [
                'name' => 'Edgars Vylcāns',
                'email' => 'vylcans@gmail.com',
                'password' => Hash::make('secret'),
                'description' => 'Vylcāns Administrators',
                'role' => \App\Models\User::ROLE_ADMIN,
                'status' => \App\Models\User::STATUS_ACTIVE,
                'gender' => \App\Models\User::GENDER_MALE,
                'birth_date' => new DateTime('1984-05-28'),
                'latitude' => 56.949649,
                'longitude' => 24.105186,
                'search_radius' => 100,
                'search_gender' => \App\Models\User::GENDER_FEMALE,
                'search_min_age' => 18,
                'search_max_age' => 100,
                'facebook_id' => 100002060510114
            ];
        }

        $faker = Faker::create('lv_LV');

        for ($i = 0; $i < 30; $i++) {

            $gender = \App\Models\User::GENDER_FEMALE;
            $searchGender = \App\Models\User::GENDER_MALE;

            if ($i % 2 == 0) {
                $gender = \App\Models\User::GENDER_MALE;
                $searchGender = \App\Models\User::GENDER_FEMALE;
            }

            $year = rand(1960, 2000);
            $month = rand(1, 12);
            $day = rand(1, 28);

            $users[] = [
                'name' => $faker->firstName . ' ' . $faker->lastName,
                'email' => $faker->email,
                'password' => Hash::make('secret'),
                'description' => $faker->sentence(),
                'role' => \App\Models\User::ROLE_USER,
                'status' => \App\Models\User::STATUS_ACTIVE,
                'gender' => $gender,
                'birth_date' => new DateTime("$year-$month-$day"),
                'latitude' => 56.949649,
                'longitude' => 24.105186,
                'search_radius' => rand(3, 100),
                'search_gender' => $searchGender,
                'search_min_age' => rand(18, 24),
                'search_max_age' => rand(25, 50),
                'facebook_id' => rand()
            ];
        }

        foreach ($users as $user) {
            \App\Models\User::create($user);
        }

    }

}