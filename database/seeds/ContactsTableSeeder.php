<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class ContactsTableSeeder extends Seeder
{

    public function run()
    {

        $me = \App\Models\User::where('facebook_id', '=', '1089618621116811')->first();

        if ($me) {
            $users = \App\Models\User::where('role', '=', \App\Models\User::ROLE_USER)
                ->where('id', '!=', $me->id)
                ->limit(15)
                ->get();

            foreach ($users as $user) {
                \App\Models\Contact::create([
                    'user_id' => $me->id,
                    'contact_id' => $user->id
                ]);
            }
        }

    }

}