<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class AllEventsTableSeeder extends Seeder
{

    public function run()
    {

        $me = \App\Models\User::where('facebook_id', '=', '1089618621116811')->first();

        if ($me) {
            $users = \App\Models\User::where('role', '=', \App\Models\User::ROLE_USER)
                ->where('id', '!=', $me->id)
                ->limit(15)
                ->get();

            foreach ($users as $user) {
                $faker = Faker::create();

                for ($i = 1; $i < 30; $i++) {
                    \App\Models\Event::create([
                        'date' => new DateTime("+$i day"),
                        'location' => $faker->company,
                        'address' => $faker->address,
                        'description' => $faker->sentence(),
                        'user_id' => $user->id,
                    ]);
                }
            }
        }

    }

}