<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class EventMessagesTableSeeder extends Seeder
{

    public function run()
    {

        $events = \App\Models\Event::whereHas('candidates', function ($q) {
            $q->where('event_candidates.status', '=', \App\Models\EventCandidate::STATUS_APPROVED);
        })
            ->get();

        $faker = Faker::create();

        foreach ($events as $event) {
            $candidates = $event
                ->candidates()
                ->where('event_candidates.status', '=', \App\Models\EventCandidate::STATUS_APPROVED)
                ->get()
            ;

            for ($i = 0; $i < 10; $i++) {
                foreach ($candidates as $candidate) {
                    \App\Models\EventMessage::create([
                        'message' => $faker->sentence(),
                        'user_id' => $candidate->id,
                        'event_id' => $event->id
                    ]);
                }
            }
        }
    }
}
