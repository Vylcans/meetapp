<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class UserEventsTableSeeder extends Seeder
{

    public function run()
    {

        $me = \App\Models\User::where('facebook_id', '=', '1089618621116811')->first();

        if ($me) {

            $faker = Faker::create();

            for ($i = 0; $i < 30; $i++) {
                \App\Models\Event::create([
                    'date' => new DateTime('+1 day'),
                    'location' => $faker->company,
                    'address' => $faker->address,
                    'description' => $faker->sentence(),
                    'user_id' => $me->id,
                ]);
            }

        }

    }

}