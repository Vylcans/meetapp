<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('avatar')->nullable();
            $table->string('email')->nullable();
            $table->string('password', 60);
            $table->rememberToken();
            $table->string('facebook_id')->nullable();

            $table->text('description')->nullable();
            $table->smallInteger('role')->default(\App\Models\User::ROLE_USER);
            $table->smallInteger('status')->default(\App\Models\User::STATUS_ACTIVE);
            $table->tinyInteger('gender')->default(\App\Models\User::GENDER_MALE);
            $table->date('birth_date')->nullable();
            $table->decimal('latitude', 18, 12)->nullable();
            $table->decimal('longitude', 18, 12)->nullable();
            $table->integer('search_radius')->unsigned()->default(100);
            $table->tinyInteger('search_gender')->default(\App\Models\User::GENDER_FEMALE);
            $table->unsignedInteger('search_min_age')->default(18);
            $table->unsignedInteger('search_max_age')->default(99);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

}
